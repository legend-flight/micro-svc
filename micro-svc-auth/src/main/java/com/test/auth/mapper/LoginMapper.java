package com.test.auth.mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.alibaba.fastjson.JSONObject;
import com.test.shiro.entity.UserEntity;

@Mapper
public interface LoginMapper {

	int updateByPrimaryKeySelective(UserEntity record);

	int updateByPrimaryKey(UserEntity record);
	
	UserEntity queryUserByLoginName(@Param("loginName") String loginName);
	
	Set<Integer> queryPermissionByAdmin();
	
	Set<Integer> queryPermissionByUser(Integer userId);
	
	
	UserEntity findByCondition(UserEntity systemUser);

	UserEntity queryUserByOpenId(@Param("openId") String openId);
	
	List<Map<String, Object>> queryBindByMap(Map<String, Object> map);

	Integer insertWechatUser(JSONObject userInfoJSON);

	Integer deleteWechatUser(@Param("openId") String openId);

	Integer delBindInfo(Map<String, Object> map);

	Integer updateWechatUser(JSONObject userInfoJSON);

	Integer insertBindRef(Map<String, Object> map);

	Integer judgeWeChatIsBind(@Param("map") JSONObject map);
	
	
	
}
