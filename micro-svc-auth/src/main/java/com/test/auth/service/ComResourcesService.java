package com.test.auth.service;

import java.util.List;
import java.util.Set;

import com.test.auth.entity.ComResourcesEntity;



/**
 * 
 * @author yuxue
 * @date 2019-05-07
 */
public interface ComResourcesService {
	
	public int save(List<ComResourcesEntity> list);
	
	public Set<String> getLocalPermissions();
}
